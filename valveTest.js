"use strict"

const publisher = require("@mavenmachines/valve").publisher;
const config = require("./config/config.json");

var key = "vehicleData";
var filterMap = {};

async function init() {
    publisher.init(config["test"]["rabbitmq"])
    .then((events) => {
        events.on("subscribe", (packet) => {
            console.log("Subscribed", packet);
            if (filterMap[packet.forType] == null) {
                filterMap[packet.forType] = [packet.forVal];
            }
            else if (!(packet.forVal in filterMap[packet.forType])) {
                filterMap[packet.forType] = filterMap[packet.forType].concat(packet.forVal);
            }
            console.log(filterMap);
        });

        events.on("unsubscribe", (packet) => {
            console.log("Unsubscribed", packet);
            if (filterMap[packet.forType] != null) {
                if (filterMap[packet.forType].indexOf(packet.forVal) != -1) {
                    filterMap[packet.forType].splice(filterMap[packet.forType].indexOf(packet.forVal), 1);
                    if (filterMap[packet.forType].length == 0) {
                        delete filterMap[packet.forType];
                    }
                }
                else {
                    throw new Error("forVal " + packet.forVal + " not found in forType " + packet.forType);
                }
            }
            else {
                throw new Error("forType not found " + packet.forType);
            }
            console.log(filterMap);
        });
    });
    publisher.registerKey(key);
    let count = 0;
    setInterval(function() {
        if (Object.keys(filterMap).length > 0) {
            let group = Object.keys(filterMap)[Math.floor(Math.random() * Math.floor(Object.keys(filterMap).length))];
            let val = filterMap[group][Math.floor(Math.random() * Math.floor(filterMap[group].length))];
            console.log("Publish Data");
            publisher.publish(key, group, val, "TEST TEST TEST TEST");
        }
        console.log("Publish Data "+key+count);
        publisher.registerKey(key+count);
        publisher.publish(key+count, `test${count}`, `val${count}`, "TEST TEST TEST TEST");
        count += 1;
    }, 1000);
}

init();