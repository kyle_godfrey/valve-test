"use strict"

const subscriber = require("@mavenmachines/valve").subscriber;
const config = require("./config/config.json");

var key = "vehicleData"

async function init() {
    await subscriber.init(config["test"]["rabbitmq"])
    .then((dataEvents) => {
        dataEvents.on("dataUpdate", function(msgPacket) {
            console.log("Packet: ", msgPacket);
        });
    });
    subscriber.subscribeToKey(key, "TEST", "1234");
    setTimeout(function() {
        subscriber.subscribeToKey(key, "TEST", "5678");
    }, 1000);
    setTimeout(function() {
        //subscriber.unsubscribeToKey(key, "TEST", "1234");
        subscriber.subscribeToKey(key, "PITTOHIO", "Pittsburgh");
    }, 5000);
    /*setTimeout(function() {
        subscriber.unsubscribeToKey(key, "TEST", "5678");
    }, 30000);*/
    let count = 0;
    setInterval(function() {
        console.log("Subscribe Data "+key+count);
        subscriber.subscribeToKey(key+count, `test${count}`, `val${count}`);
        count += 1;
    }, 5000);
}

init();